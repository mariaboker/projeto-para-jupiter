#include <stdlib.h>
#include <stdio.h>

void calcula(int valor, int *notas, int *pedido) {
  *notas = *pedido / valor;
  *pedido = *pedido % valor;
}

int main(int argc, char *argv[]){
    if (argc < 2) {
        return 1;
    }
    else {
        int pedido = atoi(argv[1]);
        int hidrogenio, helio, gravidade, gas;
        int uranio;
        int carbono;

        calcula(50, &gas, &pedido);
        calcula(20, &uranio, &pedido);
        calcula(10, &gravidade, &pedido);
        calcula(5, &helio, &pedido);
        calcula(2, &carbono, &pedido);

        hidrogenio = pedido;

        printf("%d hidrogenio\n", hidrogenio);
        printf("%d carbono\n", carbono);
        printf("%d helio\n", helio);
        printf("%d gravidade\n", gravidade);
        printf("%d uranio\n", gravidade);
        printf("%d gas\n", gas);

        return 0;
    }
}
